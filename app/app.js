import React from 'react';
import Greeting from './greeting';
import ReactDOM from 'react-dom';

ReactDOM.render(
  <Greeting name='World!' />,
  document.getElementById('example')
)
