import React from "react";

export default React.createClass({
  render: function() {
    return (
      <div className="greeting">
        <b> Hello </b>, {this.props.name}!!!!
      </div>
    );
  },
});
