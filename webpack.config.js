module.exports = {
  context: __dirname + "/app",
  entry: {
    app: "./app.js",
    index: "./index.html"
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "react-hot-loader/webpack"
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\.html$/,
        loader: "file?name=[name].[ext]",
      }
    ]
  },
  output: {
    filename: "[name].js",
    path: __dirname + "/dist",
  }
}
